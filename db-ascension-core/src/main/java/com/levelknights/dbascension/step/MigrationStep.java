package com.levelknights.dbascension.step;

import java.util.Comparator;

/**
 * @author drackowski
 */
public interface MigrationStep extends Comparable<MigrationStep> {

    int getVersion();

    String getName();

}

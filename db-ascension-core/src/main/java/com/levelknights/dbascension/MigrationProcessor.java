package com.levelknights.dbascension;

import com.levelknights.dbascension.db.DatabaseConnector;
import com.levelknights.dbascension.exception.DatabaseIsInNewerVersionException;
import com.levelknights.dbascension.exception.NotUniqueMigrationSteps;
import com.levelknights.dbascension.lock.MigrationLockService;
import com.levelknights.dbascension.step.MigrationStep;

import java.rmi.UnexpectedException;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author drackowski
 */
public abstract class MigrationProcessor<T extends MigrationStep> {
    private static final Logger log = LoggerFactory.getLogger(MigrationProcessor.class);

    private final TreeSet<T> migrationSteps;
    private final DatabaseConnector<MigrationStep> dbConnector;
    private final MigrationLockService migrationLockService;

    private String lockKey = null;

    public MigrationProcessor(final DatabaseConnector<MigrationStep> dbConnector, MigrationLockService migrationLockService, final TreeSet<T> migrationSteps) throws DatabaseIsInNewerVersionException, NotUniqueMigrationSteps {
        this.migrationSteps = new TreeSet<T>(migrationSteps);
        this.dbConnector = dbConnector;
        this.migrationLockService = migrationLockService;
    }

    public void process() throws DatabaseIsInNewerVersionException, NotUniqueMigrationSteps, UnexpectedException {
        boolean processed = false;
        while(!processed) {
            if (tryLockAndCheckIfSuccess()) {
                try {
                    processed = true;
                    processAll();
                } catch (DatabaseIsInNewerVersionException|NotUniqueMigrationSteps ex) {
                    throw ex;
                } catch (Exception e) {
                    throw new UnexpectedException("While processing migration", e);
                } finally {
                    migrationLockService.releaseLock(lockKey);
                }
            }
        }
    }

    private boolean tryLockAndCheckIfSuccess() {
        return (lockKey = migrationLockService.getLockKey()) != null;
    }

    private void processAll() throws Exception {
        checkIfVersionsAreUnique();
        checkIfMigrationsToExecuteAreNewerThanDatabase(dbConnector.getAllVersionsDone());

        for (T migrationStep : migrationSteps) {
            if (!dbConnector.getAllVersionsDone().contains(migrationStep.getVersion())) {
                log.info("Migration {}. Executing", migrationStep.getVersion());
                executeOne(migrationStep);
                dbConnector.stepDone(migrationStep);
            } else {
                log.info("Migration {}. Already exists", migrationStep.getVersion());
            }
        }

    }

    protected abstract void executeOne(T step);

    private void checkIfVersionsAreUnique() throws NotUniqueMigrationSteps {
        Set<Integer> stepIds = migrationSteps.stream()
                .map(MigrationStep::getVersion)
                .collect(Collectors.toSet());

        if (migrationSteps.size() != stepIds.size()) {
            throw new NotUniqueMigrationSteps();
        }
    }

    private void checkIfMigrationsToExecuteAreNewerThanDatabase(List<Integer> existingVersionIds) throws DatabaseIsInNewerVersionException {
        Integer maxExistingVersionId = existingVersionIds.stream().max(Integer::compare).orElse(Integer.MIN_VALUE);

        boolean thereIsMigrationToExecuteWithVersionLessThanMaxDatabaseVersion = migrationSteps.stream()
                .map(MigrationStep::getVersion)
                .filter(stepVersion -> !existingVersionIds.contains(stepVersion))
                .filter(versionId -> versionId < maxExistingVersionId)
                .findAny()
                .isPresent();
        if (thereIsMigrationToExecuteWithVersionLessThanMaxDatabaseVersion) {
            throw new DatabaseIsInNewerVersionException();
        }
    }

}

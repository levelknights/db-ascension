package com.levelknights.dbascension.lock;

/**
 * @author drackowski
 */
public interface MigrationLockService {

    String getLockKey();

    void releaseLock(String lockKey);

}

package com.levelknights.dbascension.exception;

/**
 * @author drackowski
 */
public class DatabaseIsInNewerVersionException extends Exception {
}

package com.levelknights.dbascension.db;

import com.levelknights.dbascension.step.MigrationStep;

import java.util.List;

/**
 * @author drackowski
 */
public interface DatabaseConnector<MigrationStep> {

    List<Integer> getAllVersionsDone();

    void stepDone(MigrationStep step);

}

package com.levelknights.dbascension;

import com.levelknights.dbascension.conf.DbAscMongoConfiguration;
import com.levelknights.dbascension.db.DatabaseConnector;
import com.levelknights.dbascension.exception.DatabaseIsInNewerVersionException;
import com.levelknights.dbascension.exception.NotUniqueMigrationSteps;
import com.levelknights.dbascension.lock.MigrationLockService;
import com.levelknights.dbascension.step.MigrationStep;
import com.levelknights.dbascension.step.MongoMigrationStep;
import com.mongodb.MongoClient;

import java.util.TreeSet;

/**
 * @author drackowski
 */
public class MongoMigrationProcessor extends MigrationProcessor<MongoMigrationStep> {

    private final MongoClient mongoClient;
    private final DbAscMongoConfiguration mongoConfiguration;

    public MongoMigrationProcessor(MongoClient mongoClient, DbAscMongoConfiguration mongoConfiguration, DatabaseConnector<MigrationStep> dbConnector, MigrationLockService migrationLockService, TreeSet<MongoMigrationStep> migrationSteps) throws DatabaseIsInNewerVersionException, NotUniqueMigrationSteps {
        super(dbConnector, migrationLockService, migrationSteps);
        this.mongoClient = mongoClient;
        this.mongoConfiguration = mongoConfiguration;
    }

    @Override
    protected void executeOne(MongoMigrationStep step) {
        step.execute(mongoClient, mongoConfiguration);
    }

}

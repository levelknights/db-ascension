package com.levelknights.dbascension.conf;

/**
 * Created by drackowski on 07.06.16.
 */
public class DbAscMongoConfiguration {

    public static final String TYPE = "type";
    public static final String TYPE_VERSION = "version";
    public static final String TYPE_LOCK = "lock";
    public static final String INSERT_DATE = "insertDate";
    public static final String VERSION_FIELD_NAME = "version";
    public static final String NAME_FIELD_NAME = "name";
    public static final String LOCK_VALUE = "lockValue";

    private String versionsCollectionName = "db_asc_version";

    private String dbName;

    public String getVersionsCollectionName() {
        return versionsCollectionName;
    }

    public DbAscMongoConfiguration setVersionsCollectionName(String versionsCollectionName) {
        this.versionsCollectionName = versionsCollectionName;
        return this;
    }

    public String getDbName() {
        return dbName;
    }

    public DbAscMongoConfiguration setDbName(String dbName) {
        this.dbName = dbName;
        return this;
    }
}

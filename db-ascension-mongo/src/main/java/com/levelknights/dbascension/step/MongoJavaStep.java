package com.levelknights.dbascension.step;

import com.levelknights.dbascension.conf.DbAscMongoConfiguration;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by drackowski on 07.06.16.
 */
public abstract class MongoJavaStep implements MongoMigrationStep {
    private static final Logger log = LoggerFactory.getLogger(MongoJavaStep.class);

    protected final int version;
    protected final String name;

    public MongoJavaStep(int version, String name) {
        this.version = version;
        this.name = name;
    }

    @Override
    public int getVersion() {
        return version;
    }

    @Override
    public void execute(MongoClient mongoClient, DbAscMongoConfiguration mongoConfiguration) {
        log.info("Executing java step: {} on db {}", getClass().getSimpleName(), mongoConfiguration.getDbName());
        execute(mongoClient.getDatabase(mongoConfiguration.getDbName()));
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int compareTo(MigrationStep o) {
        return Integer.compare(this.getVersion(), o.getVersion());
    }

    protected abstract void execute(MongoDatabase db);

}

package com.levelknights.dbascension.step;

import com.levelknights.dbascension.conf.DbAscMongoConfiguration;
import com.mongodb.MongoClient;

/**
 * @author drackowski
 */
public interface MongoMigrationStep extends MigrationStep{

    void execute(MongoClient mongoClient, DbAscMongoConfiguration mongoConfiguration);

}

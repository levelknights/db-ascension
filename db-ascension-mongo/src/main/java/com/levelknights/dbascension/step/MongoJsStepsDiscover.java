package com.levelknights.dbascension.step;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.JarURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

import static jdk.nashorn.internal.runtime.regexp.joni.Config.log;

/**
 * @author drackowski
 */
public class MongoJsStepsDiscover {

    public static TreeSet<MongoJsStep> forResourcePath(String resourcePath, Class classForResource) {
        try {
            String path = resourcePath.endsWith("/") ? resourcePath : resourcePath + "/";
            return getFileNamesInPath(path, classForResource)
                    .stream()
                    .map(simpleFileName -> {
                        String nameWithoutExtension = simpleFileName.substring(0, simpleFileName.length() - 3);
                        String version;
                        String name = "";
                        if (nameWithoutExtension.contains("__")) {
                            String[] split = nameWithoutExtension.substring(1).split("__", 2);
                            version = split[0];
                            name = split[1];
                        } else {
                            version = nameWithoutExtension.substring(1);
                            name = nameWithoutExtension;
                        }
                        String body = readBody(classForResource, path + simpleFileName);
                        return new MongoJsStep(Integer.parseInt(version), name, body);
                    })
                    .collect(Collectors.toCollection(TreeSet::new));
        } catch (IOException | URISyntaxException e) {
            throw new RuntimeException("Error while loading steps for path: " + resourcePath, e);
        }
    }

    public static String readBody(Class classForResource, String filePath) {
        InputStream resourceAsStream = classForResource.getResourceAsStream(filePath);
        if (resourceAsStream == null) {
            throw new RuntimeException("No resource: " + filePath);
        }
        Scanner s = new Scanner(resourceAsStream).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    private static List<String> getFileNamesInPath(String path, Class clazz) throws IOException, URISyntaxException {
        final List<String> result = new ArrayList<>();
        if ("jar".equals(clazz.getProtectionDomain().getCodeSource().getLocation().getProtocol())) {  // Run with JAR file
            path = path.startsWith("/") ? path.substring(1) : path;
            JarFile jar = ((JarURLConnection) (clazz.getProtectionDomain().getCodeSource().getLocation().toURI().toURL().openConnection())).getJarFile();
            final Enumeration<JarEntry> entries = jar.entries(); //gives ALL entries in jar
            while (entries.hasMoreElements()) {
                final String name = entries.nextElement().getName();
                if (name.startsWith(path) && !name.equals(path)) {
                    result.add(name.substring(path.length()));
                }
            }
            jar.close();
        } else { // Run with IDE
            URL resource = clazz.getResource(path);
            if (resource == null) {
                throw new RuntimeException("No resource: " + path);
            }
            Files.walk(Paths.get(resource.toURI()))
                    .filter(Files::isRegularFile)
                    .filter(filePath -> filePath.getFileName().toString().startsWith("V"))
                    .filter(filePath -> filePath.getFileName().toString().endsWith(".js"))
                    .map(filePath -> filePath.getFileName().toString())
                    .collect(Collectors.toCollection(() -> result));
        }

        return result;
    }
}

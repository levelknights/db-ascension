package com.levelknights.dbascension.step;

import com.levelknights.dbascension.conf.DbAscMongoConfiguration;
import com.mongodb.DB;
import com.mongodb.MongoClient;

/**
 * Created by drackowski on 07.06.16.
 */
public class MongoJsStep implements MongoMigrationStep {

    private final int version;
    private final String name;
    private final String body;

    public MongoJsStep(int version, String name, String body) {
        this.version = version;
        this.name = name;
        this.body = body;
    }

    @Override
    public int getVersion() {
        return version;
    }

    @Override
    public void execute(MongoClient mongoClient, DbAscMongoConfiguration mongoConfiguration) {
        new DB(mongoClient, mongoConfiguration.getDbName()).eval(body);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int compareTo(MigrationStep anotherValue) {
        return Integer.compare(this.getVersion(), anotherValue.getVersion());
    }

}

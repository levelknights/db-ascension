package com.levelknights.dbascension.db;

import com.levelknights.dbascension.conf.DbAscMongoConfiguration;
import com.levelknights.dbascension.step.MigrationStep;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.levelknights.dbascension.conf.DbAscMongoConfiguration.*;

/**
 * Created by drackowski on 07.06.16.
 */
public class MongoConnector implements DatabaseConnector<MigrationStep> {

    private final MongoClient mongoClient;
    private final DbAscMongoConfiguration configuration;

    public MongoConnector(MongoClient mongoClient, DbAscMongoConfiguration configuration) {
        this.mongoClient = mongoClient;
        this.configuration = configuration;
    }

    @Override
    public List<Integer> getAllVersionsDone() {
        MongoCollection<Document> collection = getVersionsCollection();
        FindIterable<Document> documents = collection.find();
        List<Integer> result = new ArrayList<>();
        for (Document document : documents) {
            if (document.containsKey(VERSION_FIELD_NAME)) {
                result.add(document.getInteger(VERSION_FIELD_NAME));
            }
        }
        return result;
    }

    @Override
    public void stepDone(MigrationStep migrationStep) {
        MongoCollection<Document> collection = getVersionsCollection();
        Document document = new Document();
        document.put(TYPE, TYPE_VERSION);
        document.put(VERSION_FIELD_NAME, migrationStep.getVersion());
        document.put(NAME_FIELD_NAME, migrationStep.getName());
        document.put(INSERT_DATE, new Date());
        collection.insertOne(document);
    }

    private MongoCollection<Document> getVersionsCollection() {
        MongoDatabase db = mongoClient.getDatabase(configuration.getDbName());
        MongoCollection<Document> collection = db.getCollection(configuration.getVersionsCollectionName());
        if (collection == null) {
            db.createCollection(configuration.getVersionsCollectionName());
            collection = db.getCollection(configuration.getVersionsCollectionName());
        }
        return collection;
    }


}

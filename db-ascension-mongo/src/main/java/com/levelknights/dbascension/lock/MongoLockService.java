package com.levelknights.dbascension.lock;

import com.levelknights.dbascension.conf.DbAscMongoConfiguration;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.UpdateResult;
import org.bson.BsonDateTime;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.UUID;

import static com.levelknights.dbascension.conf.DbAscMongoConfiguration.INSERT_DATE;
import static com.levelknights.dbascension.conf.DbAscMongoConfiguration.LOCK_VALUE;
import static com.levelknights.dbascension.conf.DbAscMongoConfiguration.TYPE;
import static com.levelknights.dbascension.conf.DbAscMongoConfiguration.TYPE_LOCK;
import static com.levelknights.dbascension.conf.DbAscMongoConfiguration.TYPE_VERSION;

/**
 * Created by drackowski on 07.06.16.
 */
public class MongoLockService implements MigrationLockService{
    private static final Logger log = LoggerFactory.getLogger(MongoLockService.class);

    private final MongoClient mongoClient;
    private final DbAscMongoConfiguration configuration;

    public MongoLockService(MongoClient mongoClient, DbAscMongoConfiguration configuration) {
        this.mongoClient = mongoClient;
        this.configuration = configuration;
    }

    @Override
    public String getLockKey() {
        MongoDatabase db = mongoClient.getDatabase(configuration.getDbName());
        MongoCollection<Document> collection = db.getCollection(configuration.getVersionsCollectionName());

        String uuid = UUID.randomUUID().toString();

        BsonDocument filter = new BsonDocument();
        filter.put(TYPE, new BsonString(TYPE_LOCK));

        Document document = new Document();
        document.put(TYPE, TYPE_LOCK);
        document.put(INSERT_DATE, new Date());
        document.put(LOCK_VALUE, uuid);

        Document update = new Document("$setOnInsert", document);

        UpdateOptions updateOptions = new UpdateOptions();
        updateOptions.upsert(true);

        UpdateResult updateResult = collection.updateOne(filter, update, updateOptions);

        BsonValue upsertedId = updateResult.getUpsertedId();
        if (upsertedId != null) {
            log.info("Lock created");
            return uuid;
        } else {
            log.info("Wait because there is lock");
            waitBecauseThereIsLock();
            return null;
        }
    }

    private void waitBecauseThereIsLock() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {}
    }

    @Override
    public void releaseLock(String lockKey) {
        log.info("Releasing lock: {}", lockKey);
        MongoDatabase db = mongoClient.getDatabase(configuration.getDbName());
        MongoCollection<Document> collection = db.getCollection(configuration.getVersionsCollectionName());

        Document document = new Document();
        document.put(TYPE, TYPE_LOCK);
        document.put(LOCK_VALUE, lockKey);

        collection.deleteOne(document);
    }
}

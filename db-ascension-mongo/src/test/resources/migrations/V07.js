db.bf_place.ensureIndex({
    "location.coordinates": "2dsphere"
});


//<!-- 1. Enklawa-->
db.bf_place.insert({
    "_id": "bf_place_56efb73a8f41e70444ccdab1",
    "_class": "bottleflap.core.model.Place",
    "objectInfo": {
        "creation": {
            "date": ISODate("2016-03-21T08:56:26.628Z"),
            "author": "bf_user_56e3a3c28f41e712946766af"
        },
        "updates": []
    },
    "name": "Enklawa",
    "location": {
        "city": "Mazowiecka 12, Warszawa, Poland",
        "coordinates": [
            52.2378177,
            21.0131344
        ]
    },
    "type": "CLUBS_AND_BARS",
    "description": "Fajny klub ;)",
    "status": "STANDARD"
});

//<!-- 2. PlanB-->
db.bf_place.insert({
    "_id": "bf_place_56efb7748f41e70444ccdad4",
    "_class": "bottleflap.core.model.Place",
    "objectInfo": {
        "creation": {
            "date": ISODate("2016-03-21T08:57:24.282Z"),
            "author": "bf_user_56e3a3c28f41e712946766af"
        },
        "updates": []
    },
    "name": "PlanB",
    "location": {
        "city": "aleja Wyzwolenia 18, Warszawa, Poland",
        "coordinates": [
            52.2199784,
            21.018735
        ]
    },
    "website": "",
    "type": "CLUBS_AND_BARS",
    "description": "jeszze fajniejszy klub ;)",
    "status": "STANDARD"
});

//<!-- 3. Foksal XVIII -->
db.bf_place.insert({
    "_id": "bf_place_56efb7a28f41e70444ccdafa",
    "_class": "bottleflap.core.model.Place",
    "objectInfo": {
        "creation": {
            "date": ISODate("2016-03-21T08:58:10.575Z"),
            "author": "bf_user_56e3a3c28f41e712946766af"
        },
        "updates": []
    },
    "name": "Foksal XVIII",
    "location": {
        "city": "Foksal 18, 00-372 Warszawa, Poland",
        "coordinates": [
            52.2335311,
            21.0197276
        ]
    },
    "type": "CLUBS_AND_BARS",
    "status": "STANDARD"
});

//<!-- 4. Fame Music club -->
db.bf_place.insert({
    "_id": "bf_place_56efd3518f41e70444cceff4",
    "_class": "bottleflap.core.model.Place",
    "objectInfo": {
        "creation": {
            "date": ISODate("2016-03-21T10:56:17.576Z"),
            "author": "bf_user_56e3a3c28f41e712946766af"
        },
        "updates": []
    },
    "name": "The Fame Music Club",
    "location": {
        "city": "Nowy Świat 21, Warszawa, Poland",
        "coordinates": [
            52.2325128,
            21.0190577999999
        ]
    },
    "type": "CLUBS_AND_BARS",
    "description": "dssdsdsdsdsd",
    "status": "STANDARD"
});

//<!-- 5. Club Stereo -->

db.bf_place.insert({
    "_id": "bf_place_56efd3de8f41e70444ccf043",
    "_class": "bottleflap.core.model.Place",
    "objectInfo": {
        "creation": {
            "date": ISODate("2016-03-21T10:58:38.854Z"),
            "author": "bf_user_56e3a3c28f41e712946766af"
        },
        "updates": []
    },
    "name": "Club Stereo",
    "location": {
        "city": "Zgoda 9, Warszawa, Poland",
        "coordinates": [
            52.2331769,
            21.011722
        ]
    },
    "type": "CLUBS_AND_BARS",
    "description": "dsds",
    "status": "STANDARD"
});

//<!-- 6. Maka i Woda -->

db.bf_place.insert({
    "_id": "bf_place_56efe49d8f41e70444ccf8f2",
    "_class": "bottleflap.core.model.Place",
    "objectInfo": {
        "creation": {
            "date": ISODate("2016-03-21T12:10:05.126Z"),
            "author": "bf_user_56e3a3c28f41e712946766af"
        },
        "updates": []
    },
    "name": "Maka i Woda",
    "location": {
        "city": "Chmielna 13, Warszawa, Poland",
        "coordinates": [
            52.2325907,
            21.0166136
        ]
    },
    "type": "RESTAURANTS_AND_CAFETERIAS",
    "status": "STANDARD"
});

//<!-- 6. Bydlo Powidlo -->

db.bf_place.insert({
    "_id": "bf_place_56efe5418f41e70444ccf976",
    "_class": "bottleflap.core.model.Place",
    "objectInfo": {
        "creation": {
            "date": ISODate("2016-03-21T12:12:49.360Z"),
            "author": "bf_user_56e3a3c28f41e712946766af"
        },
        "updates": []
    },
    "name": "Bydlo Powidlo",
    "location": {
        "city": "Kolejowa 47, 01-210 Warszawa, Poland",
        "coordinates": [
            52.2263433,
            20.9829201
        ]
    },
    "type": "RESTAURANTS_AND_CAFETERIAS",
    "status": "STANDARD"
});

//<!-- 7. Bowl Pub -->
db.bf_place.insert({
    "_id": "bf_place_56efe6188f41e70444ccfa53",
    "_class": "bottleflap.core.model.Place",
    "objectInfo": {
        "creation": {
            "date": ISODate("2016-03-21T12:16:24.648Z"),
            "author": "bf_user_56e3a3c28f41e712946766af"
        },
        "updates": []
    },
    "name": "Bowl Pub",
    "location": {
        "city": "Dominika Merliniego 5, Warszawa, Poland",
        "coordinates": [
            52.1947718,
            21.0254719
        ]
    },
    "type": "BOWLING_AND_CINEMAS",
    "status": "STANDARD"
});
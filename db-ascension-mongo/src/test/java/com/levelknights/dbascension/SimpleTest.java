package com.levelknights.dbascension;

import com.google.common.collect.Lists;
import com.levelknights.dbascension.conf.DbAscMongoConfiguration;
import com.levelknights.dbascension.db.MongoConnector;
import com.levelknights.dbascension.exception.DatabaseIsInNewerVersionException;
import com.levelknights.dbascension.exception.NotUniqueMigrationSteps;
import com.levelknights.dbascension.lock.MongoLockService;
import com.levelknights.dbascension.migrations.Migration1JavaStep;
import com.levelknights.dbascension.step.MongoJsStep;
import com.levelknights.dbascension.step.MongoJsStepsDiscover;
import com.levelknights.dbascension.step.MongoMigrationStep;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.rmi.UnexpectedException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static com.levelknights.dbascension.conf.DbAscMongoConfiguration.TYPE;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by drackowski on 07.06.16.
 */
public class SimpleTest {

    private MongoClient mongoClient;
    private String databaseName;
    private DbAscMongoConfiguration configuration;

    @Before
    public void prepareDb() {
        mongoClient = new MongoClient("localhost");
        databaseName = getClass().getSimpleName() + System.currentTimeMillis();
        mongoClient.getDatabase(databaseName);
        configuration = new DbAscMongoConfiguration().setDbName(databaseName);
    }

    @After
    public void cleanupDb() {
        mongoClient.dropDatabase(databaseName);
    }

    @Test
    public void basicTest() throws DatabaseIsInNewerVersionException, NotUniqueMigrationSteps, UnexpectedException {
        //given
        MongoConnector mongoConnector = new MongoConnector(mongoClient, configuration);
        MongoLockService mongoLockService = new MongoLockService(mongoClient, configuration);
        TreeSet<MongoMigrationStep> migrationSteps = new TreeSet<>();
        migrationSteps.add(new Migration1JavaStep(1));
        migrationSteps.add(new Migration1JavaStep(2));
        migrationSteps.add(new Migration1JavaStep(3));
        migrationSteps.add(new Migration1JavaStep(4));
        migrationSteps.add(new Migration1JavaStep(5));
        migrationSteps.add(new Migration1JavaStep(6));
        migrationSteps.add(new MongoJsStep(7, "V07", MongoJsStepsDiscover.readBody(getClass(), "/migrations/V07.js")));

        //when
        MongoMigrationProcessor processor = new MongoMigrationProcessor(mongoClient, configuration, mongoConnector, mongoLockService, migrationSteps);
        processor.process();

        //then
        MongoCollection<Document> collection = mongoClient.getDatabase(databaseName).getCollection(configuration.getVersionsCollectionName());
        FindIterable<Document> documents = collection.find();
        List<Integer> versionsDone = new ArrayList<>();
        for (Document document : documents) {
            versionsDone.add(document.getInteger(DbAscMongoConfiguration.VERSION_FIELD_NAME));
        }
        assertThat(versionsDone).containsAll(Lists.newArrayList(
                1,2,3,4,5,6,7
        ));

    }

    @Test
    public void releaseLockAfterMigrationErrorTest() {
        //given
        MongoConnector mongoConnector = new MongoConnector(mongoClient, configuration);
        MongoLockService mongoLockService = new MongoLockService(mongoClient, configuration);
        TreeSet<MongoMigrationStep> migrationSteps = new TreeSet<>();
        migrationSteps.add(new Migration1JavaStep(1));
        migrationSteps.add(new MongoJsStep(2, "V07", MongoJsStepsDiscover.readBody(getClass(), "/migrations/V07.js")));
        migrationSteps.add(new MongoJsStep(3, "V07", MongoJsStepsDiscover.readBody(getClass(), "/migrations/V07.js")) {
            @Override
            public void execute(MongoClient mongoClient, DbAscMongoConfiguration mongoConfiguration) {
                throw new RuntimeException("Exception for test! haha");
            }
        });

        //when
        boolean exceptionExists = false;
        try {
            MongoMigrationProcessor processor = new MongoMigrationProcessor(mongoClient, configuration, mongoConnector, mongoLockService, migrationSteps);
            processor.process();
        } catch(Exception ex) {
            exceptionExists = true;
            // om nom nom
        }

        //then
        assertThat(exceptionExists);

        MongoCollection<Document> collection = mongoClient.getDatabase(databaseName).getCollection(configuration.getVersionsCollectionName());
        ArrayList<Document> documents = Lists.newArrayList(collection.find());

        List<Integer> versionsDone = documents.stream()
                .filter(d -> DbAscMongoConfiguration.TYPE_VERSION.equals(d.getString(TYPE)))
                .map(d -> d.getInteger(DbAscMongoConfiguration.VERSION_FIELD_NAME))
                .collect(Collectors.toList());
        assertThat(versionsDone).containsAll(Lists.newArrayList(
                1,2
        ));

        long locksCount = documents.stream()
                .filter(d -> DbAscMongoConfiguration.TYPE_LOCK.equals(d.getString(TYPE)))
                .count();
        assertThat(locksCount).isZero();

    }

}

package com.levelknights.dbascension;

import com.google.common.collect.Lists;
import com.levelknights.dbascension.conf.DbAscMongoConfiguration;
import com.levelknights.dbascension.db.MongoConnector;
import com.levelknights.dbascension.exception.DatabaseIsInNewerVersionException;
import com.levelknights.dbascension.exception.NotUniqueMigrationSteps;
import com.levelknights.dbascension.lock.MongoLockService;
import com.levelknights.dbascension.migrations.Migration1JavaStep;
import com.levelknights.dbascension.step.MongoJsStep;
import com.levelknights.dbascension.step.MongoJsStepsDiscover;
import com.levelknights.dbascension.step.MongoMigrationStep;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.rmi.UnexpectedException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author drackowski
 */
public class MongoJsStepDiscoverTest {

    private MongoClient mongoClient;
    private String databaseName;
    private DbAscMongoConfiguration configuration;

    @Before
    public void prepareDb() {
        mongoClient = new MongoClient("localhost");
        databaseName = getClass().getSimpleName() + System.currentTimeMillis();
        mongoClient.getDatabase(databaseName);
        configuration = new DbAscMongoConfiguration().setDbName(databaseName);
    }

    @After
    public void cleanupDb() {
        mongoClient.dropDatabase(databaseName);
    }

    @Test
    public void testBasicState() {
        //when
        TreeSet<MongoJsStep> mongoJsSteps = MongoJsStepsDiscover.forResourcePath("/migrations", SimpleTest.class);

        //then
        assertThat(mongoJsSteps).hasSize(2);

        Iterator<MongoJsStep> iterator = mongoJsSteps.iterator();
        MongoJsStep step1 = iterator.next();
        assertThat(step1.getVersion()).isEqualTo(4);
        assertThat(step1.getName()).isEqualTo("jakas_nazwa_pliku");

        MongoJsStep step2 = iterator.next();
        assertThat(step2.getVersion()).isEqualTo(7);
        assertThat(step2.getName()).isEqualTo("V07");
    }


    @Test
    public void basicTest() throws DatabaseIsInNewerVersionException, NotUniqueMigrationSteps, UnexpectedException {
        //given
        MongoConnector mongoConnector = new MongoConnector(mongoClient, configuration);
        MongoLockService mongoLockService = new MongoLockService(mongoClient, configuration);
        TreeSet<MongoMigrationStep> migrationSteps = new TreeSet<>();
        migrationSteps.addAll(MongoJsStepsDiscover.forResourcePath("/migrations/", SimpleTest.class));

        //when
        MongoMigrationProcessor processor = new MongoMigrationProcessor(mongoClient, configuration, mongoConnector, mongoLockService, migrationSteps);
        processor.process();

        //then
        MongoCollection<Document> collection = mongoClient.getDatabase(databaseName).getCollection(configuration.getVersionsCollectionName());
        FindIterable<Document> documents = collection.find();
        List<Integer> versionsDone = new ArrayList<>();
        for (Document document : documents) {
            versionsDone.add(document.getInteger(DbAscMongoConfiguration.VERSION_FIELD_NAME));
        }
        assertThat(versionsDone).containsAll(Lists.newArrayList(
                4,7
        ));

    }


}

package com.levelknights.dbascension.migrations;

import com.levelknights.dbascension.step.MongoJavaStep;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

/**
 * @author drackowski
 */
public class Migration1JavaStep extends MongoJavaStep {

    public Migration1JavaStep(int version) {
        super(version, Migration1JavaStep.class.getName());
    }

    @Override
    protected void execute(MongoDatabase db) {
        MongoCollection<Document> costam = db.getCollection("costam");
        Document document = new Document();
        document.put("mig", "V" + version);
        document.put("name", "Name " + System.currentTimeMillis());
        costam.insertOne(document);
    }

}

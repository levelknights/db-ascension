pipeline {
    agent {
        label 'master'
    }
    environment {
        TAG_PREFIX = "release/"
    }
    stages {
        stage('Build') {
            tools {
                jdk 'Java'
            }
            steps {
                sh 'mvn -DskipTests clean package'
            }
        }

        stage('Test') {
            tools {
                jdk 'Java'
            }
            steps {
                sh 'mvn clean #TODO verify'
            }
        }

        stage('Release') {
            steps {
                sh "git fetch origin && git flow init --default --tag $TAG_PREFIX"

                sh '''mvn \
                    build-helper:parse-version \
                    versions:set -DnewVersion=\\${parsedVersion.majorVersion}.\\${parsedVersion.minorVersion}.\\${parsedVersion.incrementalVersion}'''
                script {
                    env.RELEASE_VERSION = readMavenPom().getVersion()
                    env.RELEASE_TAG_NAME = "${env.TAG_PREFIX}${env.RELEASE_VERSION}"
                }
                sh 'mvn versions:revert'

                sh '''git flow release start ${RELEASE_VERSION} && \
                      mvn versions:set -DnewVersion=$RELEASE_VERSION versions:commit && \
                      git add -A && git commit -m"change version to ${RELEASE_VERSION} (by Jenkins)"'''

                sh '''export GIT_MERGE_AUTOEDIT=no
                      git flow release finish -m "release version ${RELEASE_VERSION}" "${RELEASE_VERSION}"
                      export GIT_MERGE_AUTOEDIT=""'''

                sh '''mvn \
                    build-helper:parse-version \
                    versions:set -DnewVersion=\\${parsedVersion.majorVersion}.\\${parsedVersion.nextMinorVersion}.0-SNAPSHOT \
                    versions:commit && \
                    git add -A && git commit -m"new working version (by Jenkins)"'''

                sh '''git push --set-upstream origin develop && git push --tags origin && git push --all origin'''

                sh '''git checkout -q ${RELEASE_TAG_NAME} && \
                      mvn deploy -DskipTests'''

                addShortText "$RELEASE_VERSION"
            }
        }
    }
    post {
        always {
            cleanWs()
        }
    }
}
